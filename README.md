An implementation of tic tac toe in Tcl as a response to
https://github.com/anemator/dump/blob/master/TicTacToe.ml and a Rust 
implementation to be seen later.
Is it functional enough?  Is it Tclish enough?  Is it understandable enough?
Is it an abomination?