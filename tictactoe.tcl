namespace import ::tcl::mathop::*

set ::unoccupied { }

proc get_initial_state {} {
  list [list x o] [get_empty_grid 3 3]
}

proc get_current_actor {actors} {
    lindex $actors 0
}

proc rotate_actors {actors} {
    list {*}[lassign $actors actors] $actors
}

proc update_display {grid {note {}}} {
    puts "\n[get_grid_presentation $grid][expr {$note eq {} ? {} : "\n"}]$note"
}

proc get_grid_presentation {grid} {
    join [lmap row $grid {join $row |}] \n-----\n
}

proc get_empty_grid {rows columns} {
    lrepeat $rows [lrepeat $columns $::unoccupied]
}

proc get_column {column grid} {
    lmap row $grid {lindex $row $column}
}

proc get_row {row grid} {
    lindex $grid $row
}

proc get_list_indices {l} {
    expr {[llength $l] ? [lsearch -all $l *] : {}}
}

proc get_columns {grid} {
    lmap i [get_list_indices [get_row 0 $grid]] {get_column $i $grid}
}

proc get_top_left_diagonal {grid} {
    lmap row $grid {lindex $row [expr {[incr column] - 1}]}
}

proc get_bottom_left_diagonal {grid} {
    get_top_left_diagonal [lreverse $grid]
}

proc get_diagonals {grid} {
    list [get_top_left_diagonal $grid] [get_bottom_left_diagonal $grid]
}

proc get_win_candidates {grid} {
    list {*}$grid {*}[get_columns $grid] {*}[get_diagonals $grid]
}

proc is_won_position_for_actor {actor grid} {
    in 1 [lmap cells [get_win_candidates $grid] {eq $actor {*}$cells}]
}

proc get_winning_actor {actors grid} {
    foreach actor $actors {
        if {[is_won_position_for_actor $actor $grid]} {
            return $actor
        }
    }
}

proc handle_state {actors grid {note {}}} {
    update_display $grid $note
    if {[set winner [get_winning_actor $actors $grid]] ne {}} {
        puts "$winner won!"
        exit
    }

    set action [get_action [get_current_actor $actors]]
    set command_params [lassign $action command]
    switch $command {
        quit {
            exit
        }

        move {
            if {[is_valid_move_params $command_params]} {
              tailcall handle_move_action $actors {*}$command_params $grid
            }
        }
    }

    set note "Invalid command \"$action\""
    tailcall handle_state $actors $grid $note
}

proc get_action {actor} {
    puts -nonewline "Enter action: move column row, quit\n$actor action: "
    flush stdout
    gets stdin
}

proc is_valid_move_params {params} {
    expr {
        [string is list $params] &&
        [llength $params] == 2 &&
        [string is integer -strict [lindex $params 0]] &&
        [string is integer -strict [lindex $params end]]
    }
}

proc handle_move_action {actors column row grid} {
    set actor [get_current_actor $actors]
    if {[lindex $grid $row $column] eq $::unoccupied} {
        set next_grid [move $actor $column $row $grid]
        tailcall handle_state [rotate_actors $actors] $next_grid
    } else {
      set note "($column, $row) is an invalid move for $actor!"
      tailcall handle_state $actors $grid $note
    }
}

proc move {actor column row grid} {
    lset grid $row $column $actor
}

handle_state {*}[get_initial_state]